const state = () => ({
    result: {
        chats: [],
        publicChats: []
    }
});

export default (options) => ({
    namespaced: true,

    state: state(options),

    mutations: {
        SET_RESULT: (state, payload) => {
            console.error('payload', payload)
            state.result = payload
        }
    },

    actions: {
        setResult({ commit }, payload) {
            commit('SET_RESULT', payload)
        }
    },

    getters: {
    }
});
