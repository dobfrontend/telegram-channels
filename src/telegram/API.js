import EventEmitter from 'eventemitter3';
import TdClient from 'tdweb';
import packageJson from '../../package.json';
import { stringToBoolean, getBrowser, getOSName } from '../utils/commons';
import {
    WASM_FILE_HASH,
    WASM_FILE_NAME
} from '../../constants';

const REACT_APP_TELEGRAM_API_ID = 1377939;
const REACT_APP_TELEGRAM_API_HASH = '9d300ba0f0656c683b0c69bfc5c47227';

function databaseExists(dbname, callback) {
    var req = indexedDB.open(dbname);
    var existed = true;
    req.onsuccess = function() {
        req.result.close();
        if (!existed) indexedDB.deleteDatabase(dbname);
        callback(existed);
    };
    req.onupgradeneeded = function() {
        existed = false;
    };
}


const parameters = {
    useTestDC: false,
    readOnly: false,
    verbosity: 1,
    jsVerbosity: 3,
    fastUpdating: true,
    useDatabase: false,
    mode: 'wasm'
};

const { verbosity, jsVerbosity, useTestDC, readOnly, fastUpdating, useDatabase, mode } = parameters;

class TelegramApi extends EventEmitter {
    init() {
        this.parameters = parameters;
        
        const dbName = useTestDC ? 'tdlib_test' : 'tdlib';
        
        let options = {
            logVerbosityLevel: verbosity,
            jsLogVerbosityLevel: jsVerbosity,
            mode: mode, // 'wasm-streaming'/'wasm'/'asmjs'
            prefix: useTestDC ? 'tdlib_test' : 'tdlib',
            readOnly: readOnly,
            isBackground: false,
            useDatabase: useDatabase,
            wasmUrl: `${WASM_FILE_NAME}?_sw-precache=${WASM_FILE_HASH}`
            // onUpdate: update => this.emit('update', update)
        };

        databaseExists(dbName, exists => {
            this.client = new TdClient(options);

            this.client.onUpdate = update => {
                this.emit('update', update)
            };
        });
    }

    send(request) {
        if (!this.client) {
            console.log('send (none init)', request);
            return;
        }

        console.log('send', request);

        return this.client
            .send(request)
            .then(result => {
                console.log('send result', result);
                return result;
            })
            .catch(error => {
                console.error('send error', error);
                throw error;
            });

    }

    clientUpdate(options) {
        console.error(options)
    }

    sendTdParameters = async () => {
        const apiId = REACT_APP_TELEGRAM_API_ID;
        const apiHash = REACT_APP_TELEGRAM_API_HASH;

        if (!apiId || !apiHash) {
            if (
                window.confirm(
                    'API id is missing!\n' +
                        'In order to obtain an API id and develop your own application ' +
                        'using the Telegram API please visit https://core.telegram.org/api/obtaining_api_id'
                )
            ) {
                window.location.href = 'https://core.telegram.org/api/obtaining_api_id';
            }
        }

        const { useTestDC } = parameters;
        const { version } = packageJson;

        this.send({
            '@type': 'setTdlibParameters',
            parameters: {
                '@type': 'tdParameters',
                use_test_dc: useTestDC,
                api_id: apiId,
                api_hash: apiHash,
                system_language_code: navigator.language || 'en',
                device_model: getBrowser(),
                system_version: getOSName(),
                application_version: version,
                use_secret_chats: false,
                use_message_database: true,
                use_file_database: false,
                database_directory: '/db',
                files_directory: '/'
            }
        });

        if (this.parameters.tag && this.parameters.tagVerbosity) {
            for (let i = 0; i < this.parameters.tag.length; i++) {
                let tag = this.parameters.tag[i];
                let tagVerbosity = this.parameters.tagVerbosity[i];

                this.send({
                    '@type': 'setLogTagVerbosityLevel',
                    tag: tag,
                    new_verbosity_level: tagVerbosity
                });
            }
        }
    };
}

export default new TelegramApi()