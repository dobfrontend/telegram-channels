import {set} from '@/utils/vuex'
import TelegramApi from '../../telegram/API';
import { CHAT_SLICE_LIMIT } from '../../../constants';

const state = () => ({
    chat_ids: [],
    list: [],
    selected: '',
});

export default (options) => ({
    namespaced: true,

    state: state(options),

    mutations: {
        SET_CHAT_IDS: set('chat_ids'),
        SET_LIST: set('list'),
        SET_SELECTED: set('selected'),
        UPDATE_CHAT: (state, payload) => {
            const chats = state.list
            const index = chats.findIndex(chat => chat.id === payload.chatId)

            if (index < 0) return chats.push(payload.value);

            chats[index] = {
                ...state.list[index],
                ...payload.value,
            }
        }
    },

    actions: {
        setChats({commit}, payload) {
            commit('SET_LIST', payload)
        },
        setSelected({commit}, payload) {
            commit('SET_SELECTED', payload)
        },
        updateChat({commit}, payload) {
            commit('UPDATE_CHAT', payload)
        },
        // TODO :: move to Bridge in main.js
        async getChats({state, commit, dispatch}, {replace = false} = {}) {
            dispatch('drawer/setLoading', true, {root:true})

            let offsetOrder = '9223372036854775807'; // 2^63 - 1
            let offsetChatId = 0;
           
            const result = await TelegramApi.send({
                '@type': 'getChats',
                chat_list: { '@type': 'chatListMain' },
                offset_chat_id: offsetChatId,
                offset_order: offsetOrder,
                limit: CHAT_SLICE_LIMIT
            }).finally(() => {
                dispatch('drawer/setLoading', false, {root:true})
            });

            console.error('result', result)

            if (replace) commit('SET_CHAT_IDS', result.chat_ids)
            else commit('SET_CHAT_IDS', [...state.chat_ids, ...result.chat_ids])
        }
    },

    getters: {
        getChat: (state) => {
            return (id) => {
                return state.list.find(chat => Number(chat.id) === Number(id));
            }
        },
        getOwnwedChats: (state) => {
            return state.chat_ids.map(id => state.list.find((chat) => chat.id === id));
        },
        getSearchResults: (state, getters, rootState) => {
            let chats = rootState.search.result.chats;
            let publicChats = rootState.search.result.publicChats;


            chats = chats.map(id => {
                const chat = state.list.find(chat => chat.id === id )
                return chat || {};
            });

            publicChats = publicChats.map(id => {
                const chat = state.list.find(chat => chat.id === id )
                return chat || {};
            });

            const result = {
                chats: {
                    title: 'Your Chats',
                    items: chats
                },
                publicChats: {
                    title: 'Global Search',
                    items: publicChats
                }
            };

            [...chats, ...publicChats].map(id => {
                const chat = state.list.find(chat => chat.id === id )
                return chat || {};
            });

            return result;

        },
        getSelected: (state) =>  state.list.find(chat => chat.id === state.selected),
        getLastMessage: (state) => (chatId) => state.list.find(chat => chat.id === chatId)?.lastMessage
    }
});
