import EventEmitter from 'eventemitter3';
import API from './API';
import telegram from './telegram/API';

let _store = null;

function getOption(name) {
    return _store.getters['telegram/getOption'](name)
}

class Bridge extends EventEmitter {
    init({ app, telegram }) {
        this.app = app;
        this.store = _store = app.vue.$store;
        this.telegram = telegram;

        this.attachEvnets();

        this.telegram.init();
    }

    attachEvnets() {
        this.telegram.on('update', this.onTelegramUpdate.bind(this));

        this.app.on(API.events.GET_MESSAGES, this.onAppEvent.bind(this))
        this.app.on(API.events.SEND_MESSAGE, this.onAppEvent.bind(this))
        this.app.on(API.events.LOG_IN, this.onAppEvent.bind(this))
        this.app.on(API.events.CONFIRMATION, this.onAppEvent.bind(this))
        this.app.on(API.events.SEARCH_QUERY_CHANGE, this.onAppEvent.bind(this))


        const storeUpdates = [
            {
                state: (state) => state.chats.chat_ids,
                handler: (newValue, oldValue) => {
                    this.onStoreUpdate({
                        type: 'chats',
                        payload: { newValue, oldValue }
                    })
                }
            }
        ]

        storeUpdates.forEach(update => {
            this.store.watch(update.state, update.handler.bind(this))
        })

    }

    onStoreUpdate({ type, payload } = {}) {
        const { newValue, oldValue } = payload;

        switch (type) {
            case 'chats': {
                const newIds = newValue.filter(el => oldValue.indexOf(el) < 0);

                newIds.forEach(async chatId => {
                    const lastMessage = this.store.getters['chats/getLastMessage'](chatId);
                    const result = await this.getMessages({
                        chatId,
                        fromId: lastMessage.id,
                    });

                    this.store.dispatch('messages/updateMessages', { chatId, messages: result.messages });
                })
                break;
            }
        }


    }

    onUpdateAuthorizationState(update) {
        switch (update.authorization_state['@type']) {
            case 'authorizationStateWaitTdlibParameters':
                this.telegram.sendTdParameters();
                break;
            case 'authorizationStateWaitEncryptionKey':
                this.telegram.send({ '@type': 'checkDatabaseEncryptionKey' });
                break;
        }
    }

    async onAppEvent(e) {
        switch (e.type) {
            case API.events.GET_MESSAGES: {
                const chatId = e.value.chatId
                const result = await this.getMessages({
                    chatId: chatId,
                    fromId: e.value.fromId
                });

                this.store.dispatch('messages/updateMessages', {
                    chatId,
                    messages: result.messages,
                    replace: e.value.replace
                });
                break;
            }
            case API.events.SEND_MESSAGE: {
                const chatId = e.value.chatId
                const result = await this.sendMessage({
                    chatId: chatId,
                    content: e.value.content
                });

                console.error(result)

                // this.store.dispatch('messages/updateMessages', {
                //     chatId,
                //     messages: result.messages,
                //     replace: e.value.replace
                // });
                break;
            }
            case API.events.SEARCH_QUERY_CHANGE: {
                this.searchQuery(e.value.query);
                break;
            }
            case API.events.LOG_IN: {
                this.telegram.send({
                    "@type": "setAuthenticationPhoneNumber",
                    phone_number: e.value.phone
                })
                    .then(result => {
                        // result.ok = true
                        this.store.dispatch('auth/setConfig', { type: 'confirmation' })
                    })

                break;
            }
            case API.events.CONFIRMATION: {
                this.telegram.send({
                    "@type": "checkAuthenticationCode",
                    code: e.value.code,
                    first_name: "A",
                    last_name: "B"
                })
                    .then(result => {
                        // result.ok = true

                        this.app.vue.$router.push('/');
                    })
                    .catch(error => {
                        console.log("error", error);
                    })
                    .finally(() => {
                        console.log("finaly");
                    });
                break
            }
            // case API.events.LOG_OUT:
            //     break;
        }
    }

    async onTelegramUpdate(update) {
        console.log('receive update', update);

        switch (update['@type']) {
            case 'updateAuthorizationState': {
                this.onUpdateAuthorizationState(update)
                break;
            }
            case 'updateUser': {
                const myId = this.store.state.telegram.options['my_id']
                if (update.user.id === myId.value) {
                    this.store.dispatch('user/setUser', update.user)
                    this.store.dispatch('chats/getChats', true)
                }
                break;
            }
            case 'updateOption': {
                this.store.dispatch('telegram/setOption', {
                    name: update.name,
                    value: update.value
                })
                break;
            }
            case 'updateNewChat': {
                const chat = update.chat
                this.store.dispatch('chats/updateChat', {
                    chatId: chat.id,
                    value: chat
                })

                break;
            }
            case 'updateChatLastMessage': {
                const chatId = update.chat_id;
                const value = {
                    lastMessage: update.last_message
                };

                if (this.store.state.chats.selected !== '' && this.store.state.chats.selected === update.chat_id) {
                    const result = await this.getMessages({
                        chatId: chatId,
                        fromId: update.last_message.id
                    });

                    this.store.dispatch('messages/updateMessages', {
                        chatId,
                        messages: result.messages,
                        replace: true
                    });
                }

                this.store.dispatch('messages/updateMessages', {
                    chatId,
                    messages: [update.last_message],
                    replace: true
                });

                this.store.dispatch('chats/updateChat', { chatId, value })
                break;
            }
        }

        this.emit('update', update);
    }

    async sendMessage({ chatId, replyToMessageId = 0, content } = {}) {
        const result = await this.telegram.send({
            '@type': 'sendMessage',
            chat_id: chatId,
            reply_to_message_id: replyToMessageId,
            input_message_content: content
        })

        this.telegram.send({
            '@type': 'viewMessages',
            chat_id: chatId,
            message_ids: [result.id]
        });

        return result;
    }

    async getMessages({ chatId, fromId, offset = 0, limit = 20 } = {}) {
        return await this.telegram.send({
            '@type': 'getChatHistory',
            chat_id: chatId,
            from_message_id: fromId,
            offset,
            limit
        })
    }

    async searchQuery(query) {

        const chats = await this.telegram.send({
            '@type': "searchChats",
            query: query,
            limit: 10
        })
        const publicChats = await this.telegram.send({
            '@type': "searchPublicChats",
            query: query,
            limit: 10
        })

        console.error('chats_publicChats', [chats.chat_ids, publicChats.chat_ids])

        this.store.dispatch('search/setResult', { chats: chats.chat_ids, publicChats: publicChats.chat_ids })
        /** 
            TODO ::

            @type: "searchMessages"
            query: "Progra"
            offset_date: 0
            offset_chat_id: 0
            offset_message_id: 0
            limit: 50
         */
    }
}

const bridge = new Bridge();

bridge.init({
    app: new API(),
    telegram
});

window.com = {
    bridge: bridge
}

export default bridge;