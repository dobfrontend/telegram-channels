import {set} from '@/utils/vuex'

const state = () => ({
    chat_ids: [],
    list: [],
    selected: {},
});

export default (options) => ({
    namespaced: true,

    state: state(options),

    mutations: {
        SET_CHAT_IDS: set('chat_ids'),
        SET_LIST: set('list'),
        SET_SELECTED: set('selected'),
        UPDATE_CHAT: (state, payload) => {
            const chats = state.list
            const index = chats.findIndex(chat => chat.id === payload.chatId)

            if (index < 0) return chats.push(payload.value);

            chats[index] = {
                ...state.list[index],
                ...payload.value,
            }
        }
    },

    actions: {
        setChats({commit}, payload) {
            commit('SET_LIST', payload)
        },
        setSelected({commit}, payload) {
            commit('SET_SELECTED', payload)
        },
        updateChat({commit}, payload) {
            commit('UPDATE_CHAT', payload)
        }
    },

    getters: {
    }
});
