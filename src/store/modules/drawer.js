import {set} from '@/utils/vuex'

const state = () => ({
    menu: true,
    loading: false
});

export default (options) => ({
    namespaced: true,

    state: state(options),

    mutations: {
        SET_MENU: set('menu'),
        SET_LOADING: set('loading'),
    },

    actions: {
        setContent({commit}, payload) {
            commit('SET_CONTENT', payload)
        },
        setMenu({commit}, payload) {
            commit('SET_MENU', payload)
        },
        setLoading({commit}, payload) {
            commit('SET_LOADING', payload)
        },
    },

    getters: {
    }
});
