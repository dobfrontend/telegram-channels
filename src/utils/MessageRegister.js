

export const getMessage = (message, data) => {
    switch (message) {
        case 'messageContactRegistered': 
            return `${data.title} just joined Telegram!`;
        case 'messageSupergroupChatCreate': 
            return `${data.title} channel created!`;
        case 'messageBasicGroupChatCreate': 
            return `${data.title} group created!`;
        default: 
            return `${message} not supported yet!`
    }
};