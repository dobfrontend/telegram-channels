import { set } from '@/utils/vuex'

const state = () => ({
    user: {
        '@type' : null,
        first_name : null,
        have_access : null,
        id : null,
        is_contact : null,
        is_mutual_contact : null,
        is_scam : null,
        is_support : null,
        is_verified : null,
        language_code : null,
        last_name : null,
        phone_number : null,
        restriction_reason : null,
        status : null,
        type : null,
        username : null,
    }
});

export default (options) => ({
    namespaced: true,
    state: state(options),
    mutations: {
        SET_USER: set('user'),
    },
    actions: {
        setUser({ commit }, payload) {
            commit('SET_USER', payload)
        },
    },
    getters: {

    }
});
