const config = {
    phone: {
        confirmationMode: false,
        phone: "",
        fieldText: "Enter Phone",
        buttonText: "Send"
    },
    confirmation: {
        confirmationMode: true,
        phone: "",
        fieldText: "We have send confirmation code. Please enter it here.",
        buttonText: "Confirm"
    }
}

const state = () => ({
    config: config.phone
});

export default (options) => ({
    namespaced: true,

    state: state(options),

    mutations: {
        SET_CONFIG: (state, payload) => {
            state.config = config[payload.type]
        },
        SET_PHONE: (state, payload) => {
            state.config.phone = payload.phone
        }
    },

    actions: {
        setConfig({ commit }, payload) {
            commit('SET_CONFIG', payload)
        },
        setPhone({ commit }, payload) {
            commit('SET_PHONE', payload)
        },
    },

    getters: {
    }
});
