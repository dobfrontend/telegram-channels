import { set, toggle } from '@/utils/vuex'

const state = () => ({
    drawer: true,
    menu: true
});

export default (options) => ({
    namespaced: true,
    state: state(options),
    mutations: {
        SET_DRAWER: set('drawer'),
        TOGGLE_DRAWER: toggle('drawer'),
        SET_MENU: set('menu'),
    },
    actions: {
        setDrawer({ commit }, payload) {
            commit('SET_DRAWER', payload)
        },
        toggleDrawer({ commit }, payload) {
            commit('TOGGLE_DRAWER', payload)
        },
        setMenu({ commit }, payload) {
            commit('SET_MENU', payload)
        },
    },
    getters: {

    }
});
