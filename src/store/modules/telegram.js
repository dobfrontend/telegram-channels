import { set } from '@/utils/vuex'

const state = () => ({
    options: {}
});

export default (options) => ({
    namespaced: true,
    state: state(options),
    mutations: {
        SET_OPTION: (state, payload) => {
            state.options[payload.name] = payload.value
        },
    },
    actions: {
        setOption({ commit }, payload) {
            commit('SET_OPTION', payload)
        },
    },
    getters: {
        getOption: state => name => state[name]?.value
    }
});
