import Vue from 'vue';

const state = () => ({
    list: {},
});

export default (options) => ({
    namespaced: true,

    state: state(options),

    mutations: {
        REMOVE_MESSAGES: (state, payload) => {
            delete state.list[payload.id];
        },
        UPDATE_MESSAGES: (state, payload) => {
            let newMessages = payload.messages

            if (!payload.replace) {
                const currentMessages = state.list[payload.chatId] || [];
                newMessages = [...currentMessages, ...payload.messages];
            }
           
            Vue.set(state.list, payload.chatId, newMessages)
        }
    },

    actions: {
        updateMessages({ commit }, payload) {
            commit('UPDATE_MESSAGES', payload)
        },
        removeMessages({ commit }, payload) {
            commit('REMOVE_MESSAGES', payload)
        }
    },

    getters: {
        getChatMessages: (state) => {
            return (id) => {
                return state.list[id];
            }
        },
        getOwnMessages: (state, getters, rootState) => {
            const result = {}

            rootState.chats.chat_ids.forEach(id => result[id] = state.list[id] || []);

            return result;
        },
        getSearchResults: (state, getters, rootState) => {
            const result = {}

            const chats = rootState.search.result.chats;
            const publicChats = rootState.search.result.publicChats;

            [...chats, ...publicChats].forEach(id => result[id] = state.list[id] || []);
            return result;

        }
    }
});
