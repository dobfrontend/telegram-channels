import Home from "../views/Home";

export default [
    {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "about" */ '../views/MessagesColumnGrid.vue')
    },
    // {
    //     path: '/about',
    //     name: 'About',
    //     component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // },
    {
        path: '/login',
        name: 'Log in',
        component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
    },
    {
        path: '/search',
        name: 'Search',
        component: () => import(/* webpackChunkName: "about" */ '../views/SearchResults.vue')
    },
    // {
    //     path: '/signup',
    //     name: 'Sign up',
    //     component: () => import(/* webpackChunkName: "about" */ '../views/Signup.vue')
    // },
    // {
    //     path: '/messages',
    //     name: 'Messages ',
    //     component: () => import(/* webpackChunkName: "about" */ '../views/MessagesColumnGrid.vue')
    // }
]
