import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import EventEmitter from 'eventemitter3'
import { EventBus } from './utils/eventBus'

Vue.config.productionTip = false

class API extends EventEmitter {

    static events = {
        // messages
        GET_MESSAGES: 'GET_MESSAGES',
        SEND_MESSAGE: 'SEND_MESSAGE',
        // auth
        LOG_OUT: 'LOG_OUT',
        LOG_IN: 'LOG_IN',
        CONFIRMATION: 'CONFIRMATION',
        // search
        SEARCH_QUERY_CHANGE: 'SEARCH_QUERY_CHANGE'
    }

    constructor() {
        super();

        this.attachEvents();

        this.vue = new Vue({
            router,
            store: store(),
            vuetify,
            render: h => h(App)
        }).$mount('#app')
    }

    attachEvents() {
        for (const event in API.events) {
            if (API.events.hasOwnProperty(event)) {
                EventBus.$on(event, e => this.emit(event, { type: event, value: e }));
            }
        }
    }
}

export default API;
